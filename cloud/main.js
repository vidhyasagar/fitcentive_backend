Parse.Cloud.define('sendCommentPush', function(req, res) {
	var receiver = req.params.receiver;
	var provider = req.params.provider;
	var query = new Parse.Query(Parse.Installation);
	query.equalTo("username", receiver);
	Parse.Push.send({
		where: query,
		data : {
			alert : provider + " has commented on your post!"
		}
	}, { 
		useMasterKey: true 
	})
	.then(function() {
		res.success("likePush sent successfully");
	}, function(error) {
		res.error("Error while sending push")
	});
});

Parse.Cloud.define('sendLikePush', function(req, res) {
	var receiver = req.params.receiver;
	var provider = req.params.provider;
	var query = new Parse.Query(Parse.Installation);
	query.equalTo("username", receiver);
	Parse.Push.send({
		where: query,
		data : {
			alert : provider + " has liked your post!"
		}
	}, { 
		useMasterKey: true 
	})
	.then(function() {
		res.success("likePush sent successfully");
	}, function(error) {
		res.error("Error while sending push")
	});
});

Parse.Cloud.define('sendFollowPush', function(req, res) {
	var receiver = req.params.receiver;
	var provider = req.params.provider;
	var query = new Parse.Query(Parse.Installation);
	query.equalTo("username", receiver);
	Parse.Push.send({
		where: query,
		data : {
			alert : provider + " has requested to follow you!"
		}
	}, { 
		useMasterKey: true 
	})
	.then(function() {
		res.success("followPush sent successfully");
	}, function(error) {
		res.error("Error while sending push")
	});
});